package com.tinygrey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * 启动报错：java.lang.ClassNotFoundException: org.springframework.boot.context.properties.ConfigurationBeanFactoryMetadata
 * 原因：springboot和springcloud版本不对应
 *
 * 启动成功访问页面：http://localhost:7001/
 *
 *  注册中心集群的目的：是害怕注册中心万一崩盘，不至于服务不能访问，可以访问其他的注册中心里面的服务
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaServer_7001 {
    public static void main(String[] args) {
        SpringApplication.run(EurekaServer_7001.class, args);
    }
}
