create database springcloud;
create table if not exists dept
(
	id int auto_increment comment '主键id'
		primary key,
	name varchar(50) null comment '部门名称',
	db_source varchar(60) null
)
comment '部门表';
