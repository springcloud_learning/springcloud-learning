create database dept;

create table dept
(
	id int auto_increment comment '主键id',
	name varchar(50) null comment '部门名称',
	db_source varchar(60) null,
	constraint dept_pk
		primary key (id)
)
comment '部门表';

