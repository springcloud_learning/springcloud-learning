package com.tinygrey.service;

import com.tinygrey.entity.Dept;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(value = "SPRINGCLOUD-PROVIDER-DEPT")
@Component
public interface DeptClintService {
    @GetMapping("/getAllDept/{id}")
    Dept queryById(@PathVariable("id") Integer id);
    @GetMapping("/getAllDept")
    List<Dept> queryALl();
}
