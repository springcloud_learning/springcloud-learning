package com.tinygrey.entity;

/*import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;*/
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 部门表(Dept)表实体类
 *
 * @author madison
 * @since 2021-03-18 16:32:23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)//链式写法
public class Dept implements Serializable {
    private static final long serialVersionUID = 1L;

  /*  @TableId(type = IdType.AUTO)*/
    /*主键id*/
    private Integer id;
    /**
     * 部门名称
     */
    private String name;
  /**
   * 这个数据是存在哪一个数据库的字段，微服务，一个服务对应一个数据库，同一个信息存在不同的数据库
   */
  private String dbSource;


 /*   *//**
     * 获取主键值
     *
     * @return 主键值
     *//*
    @Override
    protected Serializable pkVal() {
        return this.id;
    }*/
}
