package com.tinygrey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * 报错：Failed to configure a DataSource: 'url' attribute is not specified and no embedded datasource could be configured.
 * 原因：是我的springcloud-api模块里面引入了mybatis-plus-boot-starter 依赖 springboot自动配置了数据库相关属性，但是Consumer模块不需要数据库，才出现报错
 * 解决办法：@SpringBootApplication() 加上排除自动装配数据库相关配置exclude = DataSourceAutoConfiguration.class
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableEurekaClient
@EnableFeignClients(basePackages = {"com.tinygrey"})
public class ConsumerFeignApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerFeignApplication.class, args);
    }
}
