package com.tinygrey.controller;

import com.tinygrey.entity.Dept;
import com.tinygrey.service.DeptClintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("consumer")
@SuppressWarnings("unchecked")//解决警告：使用了未经检查或不安全的操作
public class DeptConsumerFeignController {

    @Autowired
    DeptClintService deptClintService;

    @RequestMapping("getAllDept")
    public List<Dept> getAllDept(){
        return this.deptClintService.queryALl();
    }

    @RequestMapping("getAllDept/{id}")
    public Dept getDeptById(@PathVariable Integer id) {
        return this.deptClintService.queryById(id);
    }
}
