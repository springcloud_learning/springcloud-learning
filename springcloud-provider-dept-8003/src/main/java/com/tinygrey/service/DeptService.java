package com.tinygrey.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tinygrey.entity.Dept;

/**
 * 部门表(Dept)表服务接口
 *
 * @author madison
 * @since 2021-03-18 16:48:30
 */
public interface DeptService extends IService<Dept> {

}
