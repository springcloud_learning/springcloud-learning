package com.tinygrey.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tinygrey.entity.Dept;
import com.tinygrey.service.DeptService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * 部门表(Dept)表控制层
 *
 * @author madison
 * @since 2021-03-18 16:48:29
 */
@RestController
@RequestMapping("dept")
public class DeptController{
    private static final Logger log = LoggerFactory.getLogger(DeptController.class);
    /**
     * 服务对象
     */
    @Resource
    private DeptService deptService;

    @Autowired
    private DiscoveryClient client;

    /**
     * 分页查询所有数据
     *
     * @param page 分页对象
     * @param dept 查询实体
     * @return 所有数据
     */
    @GetMapping
    public  List<Dept> selectAll(Page<Dept> page, Dept dept) {
        Page<Dept> page1 = this.deptService.page(page, new QueryWrapper<Dept>(dept));
        return page1.getRecords();
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public Dept selectOne(@PathVariable Serializable id) {
        return this.deptService.getById(id);
    }

    /**
     * 新增数据
     *
     * @param dept 实体对象
     * @return 新增结果
     */
    @PostMapping
    public boolean insert(@RequestBody Dept dept) {
        return this.deptService.save(dept);
    }

    /**
     * 修改数据
     *
     * @param dept 实体对象
     * @return 修改结果
     */
    @PutMapping
    public boolean update(@RequestBody Dept dept) {
        return this.deptService.updateById(dept);
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping
    public boolean delete(@RequestParam("idList") List<Long> idList) {
        return this.deptService.removeByIds(idList);
    }

    /**
     * 注册进来的服务，获取一些信息
     * @return
     */
    @GetMapping("discovery")
    public List<ServiceInstance> getDiscovery() {
        //得到一个具体的注册进来的服务信息  通过具体的服务id，=====>ApplicationName
        List<ServiceInstance> instances = client.getInstances("springcloud-provider-dept-8001");
        for (ServiceInstance instance : instances) {
            System.out.println(
                    instance.getHost() + "===" +
                            instance.getInstanceId() + "===" +
                            instance.getScheme() + "===" +
                            instance.getServiceId() + "===" +
                            instance.getMetadata() + "===" +
                            instance.getPort() + "===" +
                            instance.getUri() + "===" +
                            instance.getClass() + "==="
            );
        }
        return instances;
    }
}
