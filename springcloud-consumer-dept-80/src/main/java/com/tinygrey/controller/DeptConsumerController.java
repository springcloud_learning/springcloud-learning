package com.tinygrey.controller;

import com.tinygrey.entity.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("consumer")
@SuppressWarnings("unchecked")//解决警告：使用了未经检查或不安全的操作
public class DeptConsumerController {
    //使用Ribbon负载均衡的时候不应写死url  url应该是变量，通过注册到Eureka里面服务名称取访问
    //private static final String PROVIDER_URL = "http://localhost:8001/dept";
    private static final String PROVIDER_URL = "http://SPRINGCLOUD-PROVIDER-DEPT";

    //理解消费者不应该有service层
    //提供多种便捷访问远程http的方法，简单的Restful服务模板
    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("getAllDept")
    public List<Dept> getAllDept(){
        return restTemplate.getForObject(PROVIDER_URL + "/dept", List.class);
    }

    @RequestMapping("getAllDept/{id}")
    public Dept getDeptById(@PathVariable Integer id) {
        return restTemplate.getForObject(PROVIDER_URL + "/dept" + "/" + id, Dept.class);
    }
}
