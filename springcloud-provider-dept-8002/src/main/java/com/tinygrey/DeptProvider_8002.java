package com.tinygrey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @Class:
 * @Description: DeptProvider_8001$
 * @title: DeptProvider_8001
 * @Author qlh
 * @Date: 2021/3/18 16:56
 * @Version 1.0
 */
@SpringBootApplication
@EnableEurekaClient//在服务启动后自动注入Eureka中
public class DeptProvider_8002 {
    public static void main(String[] args) {
        SpringApplication.run(DeptProvider_8002.class, args);
    }
}
