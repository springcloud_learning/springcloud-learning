package com.tinygrey.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tinygrey.entity.Dept;
import com.tinygrey.mapper.DeptMapper;
import com.tinygrey.service.DeptService;
import org.springframework.stereotype.Service;

/**
 * 部门表(Dept)表服务实现类
 *
 * @author madison
 * @since 2021-03-18 16:48:30
 */
@Service("deptService")
public class DeptServiceImpl extends ServiceImpl<DeptMapper, Dept> implements DeptService {

}
