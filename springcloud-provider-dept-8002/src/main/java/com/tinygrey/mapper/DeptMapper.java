package com.tinygrey.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tinygrey.entity.Dept;
import org.apache.ibatis.annotations.Mapper;

/**
 * 部门表(Dept)表数据库访问层
 *
 * @author madison
 * @since 2021-03-18 16:48:28
 */
@Mapper
public interface DeptMapper extends BaseMapper<Dept> {

}
